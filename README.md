# OpenML dataset: hungarian-chickenpox

https://www.openml.org/d/42999

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A spatio-temporal dataset of weekly chickenpox cases from Hungary.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42999) of an [OpenML dataset](https://www.openml.org/d/42999). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42999/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42999/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42999/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

